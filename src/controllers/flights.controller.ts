import {JsonController, Get, Post, Param, Body} from 'routing-controllers'
import { FlightsService } from '../services/flights.service'

const flightsService = new FlightsService()

@JsonController('/flights', { transformResponse: false })
export default class FlightsController {
    @Get('')
    async getAll() {

        return {
            status: 200,
            data: await flightsService.getAll(),
        }
    }

    @Post('/:code/book')
    async bookFlight(@Param('code') code:string, @Body() person: any) {
        const data = await flightsService.bookFlight(code, person);

        return {
            status: 201,
            data: data,
        }
    }
}
